
#include "stdafx.h"
#include "Win32P2.h"

#define GetStockBrush(i) ((HBRUSH)GetStockObject(i))
#define DIVISIONS 8
#define MoveTo(hdc, x, y) MoveToEx(hdc, x, y, NULL)

#define MAX_LOADSTRING 100
 HWND hwndPlayer1;
 HWND hwndPlayer2;
 HWND hwndRullse;
 HWND hwndAbout;
 HWND hwndExit;
HWND hWnd;
 static int cxBlock, cyBlock;
 RECT rect;
 char menu=0;
 short dash[3]={0,0,0},dashmax;
 bool c=0,game=0;
 char f[10][10]={
						 {9,9,9,9,9,9,9,9,9,9},
						 {9,0,0,0,0,0,1,1,1,9},
						 {9,0,0,0,0,0,1,1,1,9},
						 {9,0,0,0,0,0,1,1,1,9},
						 {9,0,0,0,0,0,0,0,0,9},
						 {9,0,0,0,0,0,0,0,0,9},
						 {9,3,3,3,0,0,0,0,0,9},
						 {9,3,3,3,0,0,0,0,0,9},
						 {9,3,3,3,0,0,0,0,0,9},
						 {9,9,9,9,9,9,9,9,9,9},
				},				 		 
				 xo=1,yo=1;

   int z[8][8]={
						 {357,492,613,720,858,912,962,1008},
						 {348,477,592,693,780,825,912,962},
						 {325,448,557,652,733,800,825,912},
						 {288,405,508,597,672,733,780,858},
						 {237,348,445,528,597,652,693,720},
						 {172,277,368,445,508,557,592,613},
						 {93,192,277,348,405,448,477,492},
						 {0,93,172,237,288,325,348,357}
				},maxsum=0;

   struct square{
   char x;
   char y;
   } comp[9]={{8,1},{8,2},{8,3},{7,1},{7,2},{7,3},{6,1},{6,2},{6,3}},
	   past,next,pas;



void LEAP(char x,char y)
{
	if (((f[x+1][y]==1)||(f[x+1][y]==3))&&(f[x+2][y]==0))
	 {
		 f[x+2][y]=7;
			   LEAP(x+2, y);
	 }
	 	 if (((f[x-1][y]==1)||(f[x-1][y]==3))&&(f[x-2][y]==0))
	 {
		 f[x-2][y]=7;
			   LEAP(x-2, y);
              
	 }
		 	 if (((f[x][y+1]==1)||(f[x][y+1]==3))&&(f[x][y+2]==0))
	 {
		 f[x][y+2]=7;		 
              LEAP(x, y+2);
	 }
			 	 if (((f[x][y-1]==1)||(f[x][y-1]==3))&&(f[x][y-2]==0))
	 {
		 f[x][y-2]=7;
			   LEAP(x, y-2);
              
	 }
}

 void MOVE (char x, char y)
 {
 
	 if (f[x+1][y]==0)
	 {
		 f[x+1][y]=7;
	 }
	 	 if (f[x-1][y]==0)
	 {
		 f[x-1][y]=7;
	 }
		 if (f[x][y+1]==0)
	 {
		 f[x][y+1]=7;
	 }
		 if (f[x][y-1]==0)
	 {
		 f[x][y-1]=7;

	 }
				 LEAP(x, y);
 }

void DELLEAP(char x,char y)
{ 
	if (f[x][y]==7) f[x][y]=0;
	if (((f[x+1][y]==1)||(f[x+1][y]==3))&&(f[x+2][y]==7))
	 {
		 f[x+2][y]=0;
			   DELLEAP(x+2, y);
	 }
	 	 if (((f[x-1][y]==1)||(f[x-1][y]==3))&&(f[x-2][y]==7))
	 {
		 f[x-2][y]=0;
			   DELLEAP(x-2, y);
              
	 }
		 	 if (((f[x][y+1]==1)||(f[x][y+1]==3))&&(f[x][y+2]==7))
	 {
		 f[x][y+2]=0;		 
             DELLEAP(x, y+2);
	 }
			 	 if (((f[x][y-1]==1)||(f[x][y-1]==3))&&(f[x][y-2]==7))
	 {
		 f[x][y-2]=0;
			   DELLEAP(x, y-2);
              
	 }
}

 void DELMOVE (char x, char y)
 {
 
	 if (f[x][y]==7) f[x][y]=0;
	 if (f[x+1][y]==7)
	 {
		 f[x+1][y]=0;
	 }
	 	 if (f[x-1][y]==7)
	 {
		 f[x-1][y]=0;
	 }
		 	 if (f[x][y+1]==7)
	 {
		 f[x][y+1]=0;
	 }
			 	 if (f[x][y-1]==7)
	 {
		 f[x][y-1]=0;

	 }
				 DELLEAP(x, y);
 }

 char FIN()
 {bool p=1,k=1;
	 for(int i = 1; i < 4; i++)
     for(int j = 6; j < 9; j++)
	 {

	 if (f[j][i]!=1)
		 p=0;
		 
	 if (f[i][j]!=3)
		 k=0;

	 }

	 if (p) return 1;
	 if (k) return 3;
	 return 9;
}

 void MOVEi(char x,char y,char n,int i, char nx,char ny,char ox,char oy);

void AI(int n,char nx, char ny,char ox,char oy)
{	
	for (int i=0;i<9;i++)
	{
		MOVEi(comp[i].x,comp[i].y,n,i,nx,ny,ox,oy);
	}
}

	
void LEAPi(char x,char y,char n,int i, char nx,char ny,char ox,char oy)
{
	if (((f[x+1][y]==1)||(f[x+1][y]==3))&&(f[x+2][y]==0))
	 {
		 if (f[x][y]==0) 		 f[x][y]=8;
		 dash[n]=z[x+1][y-1]-z[x-1][y-1];
		 comp[i].x+=2;

		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else  (n==0)?(AI(n+1,x+2,y,x,y)):(AI(n+1,nx,ny,ox,oy));

		 comp[i].x-=2;
		 dash[n]=0;
		 LEAPi(x+2, y, n, i,nx,ny,ox,oy);
		if (f[x][y]==0)  f[x][y]=0;
	 }


	 	 if (((f[x-1][y]==1)||(f[x-1][y]==3))&&(f[x-2][y]==0))
	 {
		 if (f[x][y]==0) 		 f[x][y]=8;
		 dash[n]=z[x-3][y-1]-z[x-1][y-1];
		 comp[i].x-=2;

		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else  (n==0)?(AI(n+1,x-2,y,x,y)):(AI(n+1,nx,ny,ox,oy));

		 comp[i].x+=2;
		 dash[n]=0;
			LEAPi(x-2, y, n, i,nx,ny,ox,oy);
		if (f[x][y]==0) f[x][y]=0;
	 }


		 	 if (((f[x][y+1]==1)||(f[x][y+1]==3))&&(f[x][y+2]==0))
	{
		 if (f[x][y]==0) 		f[x][y]=8;
		 dash[n]=z[x-1][y+1]-z[x-1][y-1];
		 comp[i].y+=2;

		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else  (n==0)?(AI(n+1,x,y+2,x,y)):(AI(n+1,nx,ny,ox,oy));

		 comp[i].y-=2;
		 dash[n]=0;
			   LEAPi(x, y+2, n, i,nx,ny,ox,oy);
		if (f[x][y]==0)  f[x][y]=0;
	 }


			 	 if (((f[x][y-1]==1)||(f[x][y-1]==3))&&(f[x][y-2]==0))
		 {
		if (f[x][y]==0) 			 f[x][y]=8;
		 dash[n]=z[x-1][y-3]-z[x-1][y-1];
		 comp[i].y-=2;

		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else  (n==0)?(AI(n+1,x,y-2,x,y)):(AI(n+1,nx,ny,ox,oy));
		 comp[i].y+=2;
		 dash[n]=0;
		 LEAPi(x, y-2, n, i,nx,ny,ox,oy);
		 if (f[x][y]==0) f[x][y]=0;
	 }
}

void MOVEi (char x,char y,char n,int i, char nx,char ny,char ox,char oy)
{  
		 if (f[x+1][y]==0)
	 {
		 dash[n]=z[x][y-1]-z[x-1][y-1];
		 comp[i].x+=1;

		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else  (n==0)?(AI(n+1,x+1,y,x,y)):(AI(n+1,nx,ny,ox,oy));

		 comp[i].x-=1;
		 dash[n]=0;
	 }
	 	
		 if (f[x-1][y]==0)
	 {
		 dash[n]=z[x-2][y-1]-z[x-1][y-1];
		 comp[i].x-=1;
		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 
		 else (n==0)?(AI(n+1,x-1,y,x,y)):(AI(n+1,nx,ny,ox,oy));
		 comp[i].x+=1;
		 dash[n]=0; 
	}
		 
		 if (f[x][y+1]==0)
	 {
		 dash[n]=z[x-1][y]-z[x-1][y-1];
		 comp[i].y+=1;
		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }

		 else (n==0)?(AI(n+1,x,y+1,x,y)):(AI(n+1,nx,ny,ox,oy));
		 comp[i].y-=1;
		 dash[n]=0;
	}

		if (f[x][y-1]==0)
	 {
		 dash[n]=z[x-1][y-2]-z[x-1][y-1];
		 comp[i].y-=1;
		 if (n==1) 
		 {
			 if ((dashmax<(dash[0]+dash[1]+dash[2]))&&(pas.x!=nx)&&(pas.y!=ny))
			 {
				 dashmax=dash[0]+dash[1]+dash[2];
				 past.x=ox;
				 past.y=oy;
				 next.x=nx;
				 next.y=ny;
			 }
		 }
		 else (n==0)?(AI(n+1,x,y-1,x,y)):(AI(n+1,nx,ny,ox,oy));
		 comp[i].y+=1;
		 dash[n]=0; 
	}
		LEAPi(x, y, n, i,nx,ny,ox,oy);		
}






// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HBRUSH hbrush ;

HGDIOBJ hpenOld,hbrushOld;
// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_WIN32P2, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32P2));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32P2));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_WIN32P2);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, TEXT("������"),WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU| WS_MINIMIZEBOX,
      500, 100, 640, 660, NULL, NULL, hInstance, NULL);




   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
 static BOOL fState[DIVISIONS][DIVISIONS];
 static int cxBlock, cyBlock,cy,cx;
 RECT rect;
 char x, y; 
 int p=-1;
 HFONT oldFont,newFont;    

 HINSTANCE hInst;




 switch(message)
 
 {
			case WM_SIZE :
           cxBlock = LOWORD(lParam) / DIVISIONS;
		   
           cyBlock = HIWORD(lParam) / DIVISIONS;
		   cx=LOWORD(lParam);
		   cy = HIWORD(lParam);
		   
           return 0;


			case WM_LBUTTONDOWN :
				
           x = LOWORD(lParam) / cxBlock;
           y = HIWORD(lParam) / cyBlock;


           if(x < DIVISIONS && y < DIVISIONS)
switch (menu)
				{
				case 3:{
				   if (p<6) p++;
				   if ((f[x+1][y+1]!=0)&&(f[x+1][y+1]!=3))
				   {
					   if (f[x+1][y+1]==1)
					   {
						   if (!c)
						   {
								f[x+1][y+1]=2;
								MOVE (x+1,y+1);
							    xo=x+1;
							    yo=y+1;
								c=1;
						   }
						   else
						   {
							   f[xo][yo]=1;
							   DELMOVE(xo,yo);
							   f[x+1][y+1]=2;
							   MOVE(x+1,y+1);
							   xo=x+1;
							   yo=y+1;
						   }
					   }
					   if ((f[x+1][y+1]==7)&&(c))
					   {
						   f[xo][yo]=0;
						   DELMOVE(xo,yo);
						   f[x+1][y+1]=1;
						   c=0;					   
					   }

					   if (!c)
					   {
						   past.x=0;
						   past.y=0;
						   dash[0]=dash[1]=dash[2]=dashmax=0;
						   next.x=0;
						   next.y=0;
						   maxsum=0;
						   AI(0,0,0,0,0);
						   
		   for(int i = 1; i < 9; i++)
           for(int j = 1; j < 9; j++)
		   {
			   if (f[i][j]==8) 
				   f[i][j]=0;
		   }
	
						   f[past.x][past.y]=0;						   
               rect.left = 0;
               rect.top = 0;
               rect.right =640;
               rect.bottom =660;
               InvalidateRect(hWnd, &rect, FALSE);
						   pas=past;
						   f[next.x][next.y]=3;
						   for (int u=0; u<9;u++)
							   {if ((comp[u].x==past.x)&&(comp[u].y==past.y))
								  { comp[u].x=next.x;
						   comp[u].y=next.y;}}
					   }
				   }
               rect.left = 0;
               rect.top = 0;
               rect.right =640;
               rect.bottom =660;
               InvalidateRect(hWnd, &rect, FALSE);


					  break; }
					  
		   
				case 1:
					{
						switch(game){
				case 0:{ if ((f[x+1][y+1]!=0)&&(f[x+1][y+1]!=3))
				   {
					   if (f[x+1][y+1]==1)
					   {
						   if (!c)
						   {
								f[x+1][y+1]=2;
								MOVE (x+1,y+1);
							    xo=x+1;
							    yo=y+1;
								c=1;

						   }
						   else
						   {
							   f[xo][yo]=1;
							   DELMOVE(xo,yo);
							   f[x+1][y+1]=2;
							   MOVE(x+1,y+1);
							   xo=x+1;
							   yo=y+1;
						   }
					   }
					   if ((f[x+1][y+1]==7)&&(c))
					   {
						   f[xo][yo]=0;
						  DELMOVE(xo,yo);
						   f[x+1][y+1]=1;
						   c=0;game=1;
						    
					   }
               }rect.left = 0;
				rect.top = 0;
               rect.right =660;
               rect.bottom =660;
			   InvalidateRect(hWnd, &rect, FALSE);
			   break;
					}
				case 1:
					   {
						   if ((f[x+1][y+1]!=0)&&(f[x+1][y+1]!=1))
				   {
					   if (f[x+1][y+1]==3)
					   {
						   if (!c)
						   {
								f[x+1][y+1]=2;
								MOVE (x+1,y+1);
							    xo=x+1;
							    yo=y+1;
								c=1;
						   }
						   else
						   {
							   f[xo][yo]=3;
							   DELMOVE(xo,yo);
							   f[x+1][y+1]=2;
							   MOVE(x+1,y+1);
							   xo=x+1; 
							   yo=y+1;
						   }
					   }
					   if ((f[x+1][y+1]==7)&&(c))
					   {
						   f[xo][yo]=0;
						   DELMOVE(xo,yo);
						   f[x+1][y+1]=3;
						   c=0;	game=0;				   
					   }					   
					   rect.left = 0;
               rect.top = 0;
               rect.right =660;
               rect.bottom =660;
               InvalidateRect(hWnd, &rect, FALSE);break;
					   }
					}
						} break;
					}
			}
         else
             MessageBeep(0);
		   
					break;
           return 0;
           
           case WM_PAINT :

           hdc = BeginPaint(hWnd, &ps); 


		   switch(menu)
		   {
		   case 0:
			   {
newFont = CreateFont(70,30,0,0,0,3,0,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
DEFAULT_PITCH | FF_DONTCARE,TEXT("Arial"));
 
oldFont = (HFONT)SelectObject(hdc,newFont);
				   TextOut(hdc, cx/2-110, 10, TEXT("������"), 7);
				   
		   hwndPlayer1 = CreateWindow( 
   
    L"BUTTON",  // Predefined class; Unicode assumed 
    L"���� 1�1",      // Button text 
    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
    cx/2-150,         // x position 
    cy/8+80,         // y position 
    300,        // Button width
    50,        // Button height
    hWnd,     // Parent window
    (HMENU)10000,       // No menu.
    (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), 
    NULL);      // Pointer not needed.
				ShowWindow(hwndPlayer1, SW_SHOWNORMAL);
			    hwndPlayer2 = CreateWindow( 
    L"BUTTON",  // Predefined class; Unicode assumed 
    L"���� � �����������",      // Button text 
    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
    cx/2-150,         // x position 
    cy/8*2+80,         // y position 
    300,        // Button width
    50,        // Button height
    hWnd,     // Parent window
    (HMENU)20000,       // No menu.
    (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), 
    NULL);      // Pointer not needed.
				ShowWindow(hwndPlayer2, SW_SHOWNORMAL);
			    hwndRullse = CreateWindow( 
    L"BUTTON",  // Predefined class; Unicode assumed 
    L"�������",      // Button text 
    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
    cx/2-150,         // x position 
    cy/8*3+80,         // y position 
    300,        // Button width
    50,        // Button height
    hWnd,     // Parent window
    (HMENU)30000,       // No menu.
    (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), 
    NULL);      // Pointer not needed.
				ShowWindow(hwndRullse, SW_SHOWNORMAL);
			 hwndAbout = CreateWindow( 
    L"BUTTON",  // Predefined class; Unicode assumed 
    L"������",      // Button text 
    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
    cx/2-150,         // x position 
    cy/8*4+80,         // y position 
    300,        // Button width
    50,        // Button height
    hWnd,     // Parent window
    (HMENU)40000,       // No menu.
    (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), 
    NULL);      // Pointer not needed.
				ShowWindow(hwndAbout, SW_SHOWNORMAL);
			    hwndExit = CreateWindow( 
    L"BUTTON",  // Predefined class; Unicode assumed 
    L"�����",      // Button text 
    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
    cx/2-150,         // x position 
    cy/8*5+80,         // y position 
    300,        // Button width
    50,        // Button height
    hWnd,     // Parent window
    (HMENU)50000,       // No menu.
    (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), 
    NULL);      // Pointer not needed.
				ShowWindow(hwndExit, SW_SHOWNORMAL);
			   break;
			   }
			case 1:{
		   case 3:{
		
		   //������ ����
           
		   //		  
		   
		   for(x = 0; x < DIVISIONS; x++)
           for(y = 0; y < DIVISIONS; y++)
		   {
			   
			   switch (f[x+1][y+1])
			   {
			   case 0:
		   {		 
			Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
			break;
		   }		

			   case 1:
		 {
			 Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
			hbrush =  CreateSolidBrush(RGB(180,180,180));
			hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse (hdc,x * cxBlock+2,y * cyBlock+2,(x+1) * cxBlock-2,(y+1) * cyBlock-2);			
			SelectObject(hdc, hbrushOld);
			DeleteObject(hbrush);
			break;
		   }
		 
			   case 2:
		   {		 
			   Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
		    hbrush =  CreateSolidBrush(RGB(0,100,100));
			hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse (hdc,x * cxBlock+2,y * cyBlock+2,(x+1) * cxBlock-2,(y+1) * cyBlock-2);			
			SelectObject(hdc, hbrushOld);
			DeleteObject(hbrush);
			 break;
		   }

		   
			   case 3:
		   {		 
			   Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
	   	    hbrush =  CreateSolidBrush(RGB(0,0,0));
			hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse (hdc,x * cxBlock+2,y * cyBlock+2,(x+1) * cxBlock-2,(y+1) * cyBlock-2);			
			SelectObject(hdc, hbrushOld);
			DeleteObject(hbrush); 
			break;
		   }

		   
			   case 7:
		   {		 
			   Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
	   	    hbrush =  CreateSolidBrush(RGB(350,250,150));
			hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse (hdc,x * cxBlock+22,y * cyBlock+22,(x+1) * cxBlock-22,(y+1) * cyBlock-22);			
			SelectObject(hdc, hbrushOld);
			DeleteObject(hbrush); 
			break;
		   }

		    case 8:
		   {		 
			   Rectangle(hdc, x * cxBlock, y * cyBlock, (x + 1) * cxBlock,(y + 1) * cyBlock);
	   	    hbrush =  CreateSolidBrush(RGB(350,0,0));
			hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse (hdc,x * cxBlock+22,y * cyBlock+22,(x+1) * cxBlock-22,(y+1) * cyBlock-22);			
			SelectObject(hdc, hbrushOld);
			DeleteObject(hbrush); 
			break;
		   }
		   break;
			   }}
			   }
				  if((FIN())==1)
{
MessageBox(hWnd,
(LPCWSTR)L"����� ��������",
(LPCWSTR)L"������",
MB_ICONASTERISK | MB_OK);
PostQuitMessage(0);
}

		   if((FIN())==3)
{
MessageBox(hWnd,
(LPCWSTR)L"������ ��������",
(LPCWSTR)L"������",
MB_ICONASTERISK | MB_OK);
PostQuitMessage(0);
}
		   }   
		   }
		   
  	   EndPaint(hWnd, &ps);
		   return 0;
		  case WM_COMMAND:
			  switch (LOWORD(wParam))
			  {
			case 10000:
{ 
	ShowWindow((HWND)hwndPlayer1, SW_HIDE);
	ShowWindow((HWND)hwndPlayer2, SW_HIDE);
	ShowWindow((HWND)hwndRullse, SW_HIDE);
	ShowWindow((HWND)hwndAbout, SW_HIDE);
	ShowWindow((HWND)hwndExit, SW_HIDE);
    menu=1;
	
    InvalidateRect(hWnd,NULL,true); // �������������� ����
    return 0;
}

			  case 20000:
{ 
	ShowWindow((HWND)hwndPlayer1, SW_HIDE);
	ShowWindow((HWND)hwndPlayer2, SW_HIDE);
	ShowWindow((HWND)hwndRullse, SW_HIDE);
	ShowWindow((HWND)hwndAbout, SW_HIDE);
	ShowWindow((HWND)hwndExit, SW_HIDE);
    menu=3;
	
    InvalidateRect(hWnd,NULL,true); // �������������� ����
    return 0;
}

case 30000:
{ 
	MessageBox(hWnd,
	(LPCWSTR)L"�������\n �� ������ ������ ������� ��� ����� ������� (�����, �����, �����, ������) \n �� ������ ������������� ����� ������� ����� ������ ��������� � ����� ����\n �� �������� ����� ��� ���� ������ � ������� �� ����� ����� ��������� ",
	(LPCWSTR)L"������",
	MB_ICONASTERISK | MB_OK);
	
    
    return 0;
}

case 40000:
{    
	MessageBox(hWnd,
	(LPCWSTR)L"���������� ���������� ������� ���� ��. 1-41 ����� �������. ��� ����� ��������.",
	(LPCWSTR)L"������",
	MB_ICONASTERISK | MB_OK);
	
    return 0;
}

case 50000:
{ 
	ShowWindow((HWND)hwndPlayer1, SW_HIDE);
	ShowWindow((HWND)hwndPlayer2, SW_HIDE);
	ShowWindow((HWND)hwndRullse, SW_HIDE);
	ShowWindow((HWND)hwndAbout, SW_HIDE);
	ShowWindow((HWND)hwndExit, SW_HIDE);
    
	PostQuitMessage(0);
	
    return 0;
}
}


	break;
       case WM_DESTROY :
               PostQuitMessage(0);
               return 0;
         

        default:
          return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;


}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

